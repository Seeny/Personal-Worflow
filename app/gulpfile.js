var gulp = require('gulp');
var sass= require('gulp-sass');
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');
const imagemin = require('gulp-imagemin');
var htmlmin = require('gulp-htmlmin');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();

// Static Server + watching scss/html files
gulp.task('serve', ['sass', 'compress', 'minify'], function() {

    browserSync.init({
        server: "./app"
    });
    gulp.watch("js/*.js",['compress']).on('change', browserSync.reload);
    gulp.watch("scss/**/*.scss", ['sass']);
    gulp.watch("./*.html",['minify']);
    gulp.watch("app/*.html").on('change', browserSync.reload);
});

gulp.task('compress', function () {

    gulp.src('js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('app/js'))
    
  });

gulp.task('sass',function(){
    return gulp.src('scss/**/*.scss')
        .pipe(sass())
        .pipe(cssnano())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.stream());
});

gulp.task('optimizar', function(){
    gulp.src('img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('app/img'))
    
});
   

gulp.task('minify', function() {
    return gulp.src('./*.html')
      .pipe(htmlmin({collapseWhitespace: true}))
      .pipe(gulp.dest('app'));
  });